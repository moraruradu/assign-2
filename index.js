
const FIRST_NAME = "RADU";
const LAST_NAME = "MORARU";
const GRUPA = "1092";

function initCaching() {
   var Cache = {};

   Cache.getCache = function() {
   		return this;
   }

   Cache.pageAccessCounter = function(PageName = 'home') {
   		if (Cache.hasOwnProperty(PageName.toLowerCase()))
   			Cache[PageName.toLowerCase()]++;
   		else
   			Cache[PageName.toLowerCase()] = 1;
   }

   return Cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

